package az.demo.iradvar.mapper;

import az.demo.iradvar.dto.CommentDto;
import az.demo.iradvar.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {
    CommentDto mapEntityToDto(Comment comment);

    Comment mapDtoToEntity(CommentDto commentDto);
}
