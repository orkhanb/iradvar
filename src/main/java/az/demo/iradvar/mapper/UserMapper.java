package az.demo.iradvar.mapper;

import az.demo.iradvar.dto.UserDto;
import az.demo.iradvar.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    UserDto mapEntityToDto(User user);

    User mapDtoToEntity(UserDto userDto);
}
