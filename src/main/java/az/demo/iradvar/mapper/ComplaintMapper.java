package az.demo.iradvar.mapper;

import az.demo.iradvar.dto.ComplaintDto;
import az.demo.iradvar.entity.Complaint;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ComplaintMapper {
    ComplaintDto mapEntityToDto(Complaint complaint);

    Complaint mapDtoToEntity(ComplaintDto complaintDto);
}
