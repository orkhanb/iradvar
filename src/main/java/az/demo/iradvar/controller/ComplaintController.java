package az.demo.iradvar.controller;

import az.demo.iradvar.dto.ComplaintCreateDto;
import az.demo.iradvar.dto.ComplaintDto;
import az.demo.iradvar.service.ComplaintService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/complaint")
@RequiredArgsConstructor
public class ComplaintController {

    private final ComplaintService service;

    @GetMapping("/all")
    public Set<ComplaintDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public ComplaintDto getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public ComplaintDto updateById(@PathVariable Long id,
                                   @Valid @RequestBody ComplaintCreateDto complaintDto) {
        return service.updateById(id, complaintDto);
    }

    @PostMapping()
    public ComplaintDto create(@Valid @RequestBody ComplaintCreateDto complaintDto) {
        return service.create(complaintDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    }
}
