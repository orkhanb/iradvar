package az.demo.iradvar.controller;

import az.demo.iradvar.dto.RegistrationRequestDto;
import az.demo.iradvar.dto.UserDto;
import az.demo.iradvar.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @GetMapping("/all")
    public Set<UserDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable Long id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public UserDto updateById(@PathVariable Long id,
                              @Valid @RequestBody UserDto userDto) {
        return service.updateById(id, userDto);
    }

    @PostMapping()
    public UserDto create(@Valid @RequestBody RegistrationRequestDto requestDto) {
        return service.create(requestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    }
}
