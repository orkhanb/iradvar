package az.demo.iradvar.controller;

import az.demo.iradvar.dto.CommentCreateDto;
import az.demo.iradvar.dto.CommentDto;
import az.demo.iradvar.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/comment")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService service;

    @GetMapping("/all")
    public Set<CommentDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public CommentDto getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @PutMapping("/{id}")
    public CommentDto updateById(@PathVariable Long id,
                                 @Valid @RequestBody CommentCreateDto commentDto) {
        return service.updateById(id, commentDto);
    }

    @PostMapping()
    public CommentDto create(@Valid @RequestBody CommentCreateDto commentDto) {
        return service.create(commentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    }
}
