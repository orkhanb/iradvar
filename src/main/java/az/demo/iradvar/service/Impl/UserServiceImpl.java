package az.demo.iradvar.service.Impl;

import az.demo.iradvar.auth.security.facade.AuthenticationFacade;
import az.demo.iradvar.dto.RegistrationRequestDto;
import az.demo.iradvar.dto.UserDto;
import az.demo.iradvar.entity.User;
import az.demo.iradvar.mapper.UserMapper;
import az.demo.iradvar.repository.UserRepo;
import az.demo.iradvar.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final AuthenticationFacade facade;
    private final UserRepo userRepo;
    private final UserMapper userMapper;

    @Override
    public Set<UserDto> getAll() {
        Set<UserDto> userDtos = userRepo.findAll().stream()
                .map(u -> (u != null) ? userMapper.mapEntityToDto(u) : null)
                .collect(Collectors.toSet());

        return userDtos;
    }

    @Override
    public UserDto create(RegistrationRequestDto userDto) {
        return null;
    }

    @Override
    public UserDto getById(Long id) {
        User user = userRepo.findById(id).orElseThrow(() -> new RuntimeException("User Id Not Found"));
        return userMapper.mapEntityToDto(user);
    }

    @Override
    public UserDto updateById(Long id, UserDto userDto) {
        String email = facade.getAuthentication().getName();
        User user2 = userRepo.findByEmail(email).orElseThrow(() -> new RuntimeException("Email Not Found!"));
        if (user2.getId() != id) {
            new RuntimeException("User id and mail do not match");
        }
        userDto.setId(id);
        return userMapper.mapEntityToDto(userRepo.save(userMapper.mapDtoToEntity(userDto)));
    }

    @Override
    public void deleteById(Long id) {

    }
}
