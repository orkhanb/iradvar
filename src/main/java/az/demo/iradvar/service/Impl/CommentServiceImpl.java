package az.demo.iradvar.service.Impl;

import az.demo.iradvar.dto.CommentCreateDto;
import az.demo.iradvar.dto.CommentDto;
import az.demo.iradvar.entity.Comment;
import az.demo.iradvar.entity.Complaint;
import az.demo.iradvar.mapper.CommentMapper;
import az.demo.iradvar.repository.CommentRepo;
import az.demo.iradvar.repository.ComplaintRepo;
import az.demo.iradvar.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepo commentRepo;
    private final CommentMapper commentMapper;
    private final ComplaintRepo complaintRepo;

    @Override
    @Cacheable(value = "comment", key = "#root.method.name")
    public Set<CommentDto> getAll() {
        Set<CommentDto> commentDtos = commentRepo.findAll().stream().map(entity -> (entity != null) ? commentMapper.mapEntityToDto(entity) : null).collect(Collectors.toSet());
        return commentDtos;
    }

    @Override
    @CacheEvict(value = "comment", allEntries = true)
    public CommentDto create(CommentCreateDto commentDto) {
        Complaint complaint = complaintRepo.findById(commentDto.getComplaintId()).orElseThrow(() -> new RuntimeException("Complaint with ID " + commentDto.getComplaintId().toString() + " does not exist."));
        complaint.setCommentCount(complaint.getCommentCount() + 1);
        Comment comment = Comment.builder().text(commentDto.getText()).complaint(complaint).build();
        complaintRepo.save(complaint);

        return commentMapper.mapEntityToDto(commentRepo.save(comment));
    }

    @Override
    @Cacheable(value = "comment", key = "#id")
    public CommentDto getById(Long id) {
        Comment comment = commentRepo.findById(id).orElseThrow(() -> new RuntimeException("Comment with ID " + id.toString() + " does not exist."));
        return commentMapper.mapEntityToDto(comment);
    }

    @Override
    @CachePut(value = "comment", key = "#id")
    public CommentDto updateById(Long id, CommentCreateDto commentDto) {
        Comment comment = commentRepo.findById(id).orElseThrow(() -> new RuntimeException("Comment with ID " + id.toString() + " does not exist."));
        comment.setText(comment.getText());
        return commentMapper.mapEntityToDto(commentRepo.save(comment));
    }


    @Override
    @Caching(evict = {
            @CacheEvict(value = "comment", allEntries = true),
            @CacheEvict(value = "complaint", allEntries = true)
    }
    )
    public void deleteById(Long id) {
        Optional<Complaint> complaint = complaintRepo.findById(commentRepo.findById(id).get().getComplaint().getId());
        if (complaint.isPresent()) {
            complaint.get().setCommentCount(complaint.get().getCommentCount() - 1);
            complaintRepo.save(complaint.get());
        }
        commentRepo.deleteById(id);
    }
}
