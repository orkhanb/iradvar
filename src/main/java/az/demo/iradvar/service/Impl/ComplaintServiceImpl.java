package az.demo.iradvar.service.Impl;

import az.demo.iradvar.auth.security.facade.AuthenticationFacade;
import az.demo.iradvar.dto.ComplaintCreateDto;
import az.demo.iradvar.dto.ComplaintDto;
import az.demo.iradvar.entity.Complaint;
import az.demo.iradvar.mapper.ComplaintMapper;
import az.demo.iradvar.repository.ComplaintRepo;
import az.demo.iradvar.repository.UserRepo;
import az.demo.iradvar.service.ComplaintService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ComplaintServiceImpl implements ComplaintService {

    private final AuthenticationFacade facade;
    private final ComplaintRepo complaintRepo;
    private final UserRepo userRepo;
    private final ComplaintMapper complaintMapper;

    @Override
    @Cacheable(value = "complaint", key = "#root.method.name")
    public Set<ComplaintDto> getAll() {
        Set<ComplaintDto> complaintDtos = complaintRepo.findAll()
                .stream().map(entity -> (entity != null) ? complaintMapper.mapEntityToDto(entity) : null)
                .collect(Collectors.toSet());
        return complaintDtos;
    }

    @Override
    @CacheEvict(value = "complaint", allEntries = true)
    public ComplaintDto create(ComplaintCreateDto complaintDto) {
        Complaint complaint = Complaint.builder()
                .title(complaintDto.getTitle())
                .description(complaintDto.getDescription())
                .toAddress(complaintDto.getToAddress())
                .commentCount(0)
                .likeCount(0)
                .isSolved(false)
                .isAnonim(complaintDto.getIsAnonim())
                .user(userRepo.findById(complaintDto.getUserId())
                        .orElseThrow(() -> new RuntimeException("User id not found:CreateComplaint")))
                .build();

        return complaintMapper.mapEntityToDto(complaintRepo.save(complaint));
    }

    @Override
    @Cacheable(value = "complaint", key = "#id")
    public ComplaintDto getById(Long id) {
        Complaint complaint =
                complaintRepo.findById(id)
                        .orElseThrow(() -> new RuntimeException("Complaint with ID " + id.toString() + " does not exist."));
        return complaintMapper.mapEntityToDto(complaint);
    }

    @Override
    @CachePut(value = "complaint", key = "#id")
    public ComplaintDto updateById(Long id, ComplaintCreateDto complaintDto) {
        Complaint complaint = complaintRepo.findById(id).orElseThrow(
                () -> new RuntimeException("Complaint with ID " + id.toString() + " does not exist."));
        complaint.setTitle(complaint.getTitle());
        complaint.setDescription(complaint.getDescription());
        complaint.setToAddress(complaint.getToAddress());
        complaint.setIsAnonim(complaintDto.getIsAnonim());
        complaint.setIsSolved(complaint.getIsSolved());
        complaint.setUser(userRepo.findById(complaintDto.getUserId())
                .orElseThrow(() -> new RuntimeException("User id not found:CreateComplaint")));

        return complaintMapper.mapEntityToDto(
                complaintRepo.save(complaint));
    }

    @Override
    @CacheEvict(value = "complaint", allEntries = true)
    public void deleteById(Long id) {
        complaintRepo.deleteById(id);
    }
}
