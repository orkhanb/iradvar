package az.demo.iradvar.service;

import az.demo.iradvar.dto.ComplaintCreateDto;
import az.demo.iradvar.dto.ComplaintDto;

import java.util.Set;

public interface ComplaintService {
    Set<ComplaintDto> getAll();

    ComplaintDto create(ComplaintCreateDto complaintDto);

    ComplaintDto getById(Long id);

    ComplaintDto updateById(Long id, ComplaintCreateDto complaintDto);

    void deleteById(Long id);
}
