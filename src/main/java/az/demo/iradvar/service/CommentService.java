package az.demo.iradvar.service;

import az.demo.iradvar.dto.CommentCreateDto;
import az.demo.iradvar.dto.CommentDto;

import java.util.Set;

public interface CommentService {
    Set<CommentDto> getAll();

    CommentDto create(CommentCreateDto commentDto);

    CommentDto getById(Long id);

    CommentDto updateById(Long id, CommentCreateDto commentDto);

    void deleteById(Long id);
}
