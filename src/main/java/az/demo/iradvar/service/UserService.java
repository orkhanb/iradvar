package az.demo.iradvar.service;

import az.demo.iradvar.dto.RegistrationRequestDto;
import az.demo.iradvar.dto.UserDto;

import java.util.Set;

public interface UserService {
    Set<UserDto> getAll();

    UserDto create(RegistrationRequestDto userDto);

    UserDto getById(Long id);

    UserDto updateById(Long id, UserDto userDto);

    void deleteById(Long id);
}
