package az.demo.iradvar.auth.controller;

import az.demo.iradvar.auth.security.jwt.JwtUtils;
import az.demo.iradvar.auth.security.services.UserDetailsImpl;
import az.demo.iradvar.dto.*;
import az.demo.iradvar.entity.Role;
import az.demo.iradvar.entity.User;
import az.demo.iradvar.repository.RoleRepo;
import az.demo.iradvar.repository.UserRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
@AllArgsConstructor
@Slf4j
public class AuthController {
    private final UserRepo userRepository;
    private final RoleRepo roleRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/signin")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody LoginRequestDto loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> (item != null) ? item.getAuthority() : null)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                roles));
    }


    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser(@Valid @RequestBody RegistrationRequestDto signUpRequest) {
        Boolean existLogin = userRepository.existsByEmail(signUpRequest.getEmail());
        if (Boolean.TRUE.equals(existLogin)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already taken!"));
        } else {

            User user = User.builder()
                    .email(signUpRequest.getEmail())
                    .password(encoder.encode(signUpRequest.getPassword()))
                    .build();


            Set<String> strRoles = signUpRequest.getRoles();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: Roles are not found."));
            } else {
                strRoles.forEach(role -> {
                    switch (role.toUpperCase()) {
                        case "ADMIN":
                            Role adminRole = roleRepository.findByName(ERole.ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: ADMIN role is not found."));
                            roles.add(adminRole);

                            break;
                        case "USER":
                            Role userRole = roleRepository.findByName(ERole.USER)
                                    .orElseThrow(() -> new RuntimeException("Error: USER role is not found."));
                            roles.add(userRole);
                            break;
                        case "BUSINESS":
                            Role businessRole = roleRepository.findByName(ERole.BUSINESS)
                                    .orElseThrow(() -> new RuntimeException("Error: BUSINESS role is not found."));
                            roles.add(businessRole);
                            break;
                        default:
                            Role userRole2 = roleRepository.findByName(ERole.USER)
                                    .orElseThrow(() -> new RuntimeException("Error: USER role is not found."));
                            roles.add(userRole2);
                            break;
                    }
                });
            }

            user.setRoles(roles);
            userRepository.save(user);

            return ResponseEntity.ok().body(new MessageResponse("Registered successfully."));
        }
    }


}