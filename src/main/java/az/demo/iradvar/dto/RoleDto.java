package az.demo.iradvar.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoleDto implements Serializable {
    private static final long SerialVersionUID = 123123123L;
    Long id;
    String name;
}
