package az.demo.iradvar.dto;

public enum ERole {
    ADMIN,
    USER,
    BUSINESS
}
