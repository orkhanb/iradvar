package az.demo.iradvar.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ComplaintCreateDto {
    @Size(min = 7, max = 30)
    String title;
    @NotNull
    Long userId;
    @Size(min = 7, max = 30)
    String toAddress;
    @Size(min = 7, max = 300)
    String description;
    Set<ImageDto> images;
    @Builder.Default
    Boolean isAnonim = false;
    @Builder.Default
    Boolean isSolved = false;
}
