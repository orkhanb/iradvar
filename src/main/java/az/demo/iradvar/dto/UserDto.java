package az.demo.iradvar.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class UserDto implements Serializable {

    private static final long SerialVersionUID = 123123123L;

    Long id;

    @Email
    String email;

    @NotNull
    @NotBlank
    String firstname;

    @NotNull
    @NotBlank
    String lastname;


    String password;

    byte[] image;

    Set<RoleDto> roles;

    LocalDateTime createDateTime;
}
