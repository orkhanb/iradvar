package az.demo.iradvar.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class ComplaintDto implements Serializable {

    private static final long SerialVersionUID = 123123123L;

    Long id;
    String title;
    UserDto user;
    String toAddress;
    String description;
    LocalDateTime createDateTime;
    Integer likeCount;
    Integer commentCount;
    Set<ImageDto> images;
    Boolean isSolved;
    Boolean isAnonim;
}
