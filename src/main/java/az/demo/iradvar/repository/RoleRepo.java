package az.demo.iradvar.repository;

import az.demo.iradvar.dto.ERole;
import az.demo.iradvar.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
